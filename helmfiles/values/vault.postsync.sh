#!/bin/bash

# Script to register the Kubernetes cluster to log in to Vault
# Requires Vault admin privileges

VAULT_ACCOUNT_NAME=${1}
VAULT_AUTH_NAME=${2}
KUBERNETES_NAMESPACE=${3}

VAULT_SECRET_NAME=$(kubectl get secrets -o go-template='{{ range.items }}{{ .metadata.name }}{{"\n"}}{{ end }}' --namespace $KUBERNETES_NAMESPACE | grep -m 1 "$VAULT_ACCOUNT_NAME-token")
TOKEN_REVIEW_JWT=$(kubectl get secret $VAULT_SECRET_NAME -o go-template='{{ .data.token }}' --namespace $KUBERNETES_NAMESPACE | base64 -d)
KUBE_CA_CERT=$(kubectl get secret $VAULT_SECRET_NAME -o go-template='{{ index .data "ca.crt" }}' --namespace $KUBERNETES_NAMESPACE | base64 -d)
KUBE_HOST=$(vault read -field="kubernetes_host" auth/$VAULT_AUTH_NAME/config)

vault write auth/$VAULT_AUTH_NAME/config \
    kubernetes_host="$KUBE_HOST"
    token_reviewer_jwt="$TOKEN_REVIEW_JWT" \
    kubernetes_ca_cert="$KUBE_CA_CERT"
